#include "fatorial.h"

int fatorial(int x){
	/* Escreva seu código aqui */
	if (x <= 0) {
		return -1;
	}
	else if (x == 1) {
		return 1;
	}
	else {
		return x * fatorial(x - 1);
	}
}
